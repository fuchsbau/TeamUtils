package de.fuchspfoten.teamutils;

import com.comphenix.protocol.ProtocolLibrary;
import de.fuchspfoten.teamutils.model.PermanentBan;
import de.fuchspfoten.teamutils.model.TemporaryBan;
import de.fuchspfoten.teamutils.model.Unban;
import de.fuchspfoten.teamutils.modules.BanModule;
import de.fuchspfoten.teamutils.modules.ChatClientBanModule;
import de.fuchspfoten.teamutils.modules.ClearChatModule;
import de.fuchspfoten.teamutils.modules.EchestModule;
import de.fuchspfoten.teamutils.modules.FileModule;
import de.fuchspfoten.teamutils.modules.GamemodeModule;
import de.fuchspfoten.teamutils.modules.InvseeModule;
import de.fuchspfoten.teamutils.modules.KickModule;
import de.fuchspfoten.teamutils.modules.ModBlockerModule;
import de.fuchspfoten.teamutils.modules.NotausModule;
import de.fuchspfoten.teamutils.modules.OpModule;
import de.fuchspfoten.teamutils.modules.RawModule;
import de.fuchspfoten.teamutils.modules.SayModule;
import de.fuchspfoten.teamutils.modules.StopModule;
import de.fuchspfoten.teamutils.modules.TaintCollectorModule;
import de.fuchspfoten.teamutils.modules.TeamChatModule;
import de.fuchspfoten.teamutils.modules.TeamCompassModule;
import de.fuchspfoten.teamutils.modules.UsernameCollectorModule;
import de.fuchspfoten.teamutils.modules.VanishModule;
import de.fuchspfoten.teamutils.modules.WhoisModule;
import lombok.Getter;
import me.lucko.luckperms.api.LuckPermsApi;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Plugin main class.
 */
public class TeamUtilsPlugin extends JavaPlugin implements Listener {

    /**
     * The LuckPerms permission API.
     */
    private @Getter static LuckPermsApi permissionAPI;

    /**
     * The singleton plugin instance.
     */
    private @Getter static TeamUtilsPlugin self;

    /**
     * Schedules a task with the scheduler.
     *
     * @param runnable The task.
     * @param ticks    How many ticks in the future the task will be executed.
     * @return The task ID.
     */
    public static int scheduleTask(final Runnable runnable, final long ticks) {
        return Bukkit.getScheduler().scheduleSyncDelayedTask(self, runnable, ticks);
    }

    /**
     * The OP module.
     */
    private OpModule opModule;

    /**
     * The vanish module.
     */
    private @Getter VanishModule vanishModule;

    @Override
    public void onDisable() {
        opModule.deopAll();
    }

    @Override
    public void onEnable() {
        self = this;

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Fetch APIs.
        final RegisteredServiceProvider<LuckPermsApi> provider =
                Bukkit.getServicesManager().getRegistration(LuckPermsApi.class);
        if (provider != null) {
            permissionAPI = provider.getProvider();
        } else {
            throw new IllegalStateException("Could not retrieve LuckPermsApi");
        }

        // Register messages.
        PermanentBan.registerMessages();
        TemporaryBan.registerMessages();
        Unban.registerMessages();

        // Register command executor modules.
        getCommand("cc").setExecutor(new ClearChatModule());
        getCommand("file").setExecutor(new FileModule());
        getCommand("notaus").setExecutor(new NotausModule(getConfig()));
        getCommand("raw").setExecutor(new RawModule());
        getCommand("say").setExecutor(new SayModule());
        getCommand("stop").setExecutor(new StopModule());
        getCommand("whois").setExecutor(new WhoisModule());

        // Register modules.
        getServer().getPluginManager().registerEvents(new BanModule(getConfig()), this);
        getServer().getPluginManager().registerEvents(new ChatClientBanModule(), this);
        getServer().getPluginManager().registerEvents(new EchestModule(), this);
        getServer().getPluginManager().registerEvents(new GamemodeModule(), this);
        getServer().getPluginManager().registerEvents(new InvseeModule(), this);
        getServer().getPluginManager().registerEvents(new KickModule(getConfig()), this);
        getServer().getPluginManager().registerEvents(new ModBlockerModule(), this);
        getServer().getPluginManager().registerEvents(new TaintCollectorModule(), this);
        getServer().getPluginManager().registerEvents(new TeamChatModule(), this);
        getServer().getPluginManager().registerEvents(new TeamCompassModule(), this);
        getServer().getPluginManager().registerEvents(new UsernameCollectorModule(), this);

        // Register explicit modules.
        opModule = new OpModule();
        getServer().getPluginManager().registerEvents(opModule, this);
        vanishModule = new VanishModule();
        getServer().getPluginManager().registerEvents(vanishModule, this);

        // Register protocol filters.
        ProtocolLibrary.getProtocolManager().addPacketListener(new VanishPingFilter());
    }
}
