/*
 * Copyright (c) 2019. All Rights Reserved.
 */

package de.fuchspfoten.teamutils;

import com.comphenix.packetwrapper.WrapperStatusServerServerInfo;
import com.comphenix.protocol.PacketType.Status.Server;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import org.bukkit.Bukkit;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

/**
 * Filters pings so that they do not leak information about vanished users.
 */
public class VanishPingFilter extends PacketAdapter {

    /**
     * Constructor.
     */
    public VanishPingFilter() {
        super(FuchsLibPlugin.getSelf(), ListenerPriority.NORMAL, Server.SERVER_INFO);
    }

    @Override
    public void onPacketSending(final PacketEvent event) {
        final Collection<UUID> vanished = TeamUtilsPlugin.getSelf().getVanishModule().getVanishedIDs();
        final WrapperStatusServerServerInfo packet = new WrapperStatusServerServerInfo(event.getPacket());
        packet.getJsonResponse().setPlayersOnline(Bukkit.getOnlinePlayers().size() - vanished.size());

        final Collection<WrappedGameProfile> profiles = new LinkedList<>(packet.getJsonResponse().getPlayers());
        profiles.removeIf(profile -> vanished.contains(profile.getUUID()));
        packet.getJsonResponse().setPlayers(profiles);
    }
}
