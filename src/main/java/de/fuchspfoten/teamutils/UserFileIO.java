package de.fuchspfoten.teamutils;

import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.teamutils.model.UserFileEntry;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

/**
 * Manages user file entries in player data.
 */
public final class UserFileIO {

    /**
     * A cache of user file entries.
     */
    private static final Map<UUID, List<UserFileEntry>> entryCache = new HashMap<>();

    /**
     * Adds a user file entry to the given player.
     *
     * @param uuid  The user ID.
     * @param entry The user file entry.
     */
    public static void addFileEntry(final UUID uuid, final UserFileEntry entry) {
        final PlayerData data = new PlayerData(uuid);
        if (!data.getStorage().isConfigurationSection("userfile")) {
            data.getStorage().createSection("userfile");
        }

        final ConfigurationSection file = data.getStorage().getConfigurationSection("userfile");
        final int nextKey = file.getKeys(false).size();
        final ConfigurationSection targetSection = file.createSection(String.valueOf(nextKey));
        entry.write(targetSection);

        entryCache.computeIfAbsent(uuid, x -> new ArrayList<>()).add(entry);
    }

    /**
     * Obtains the user file entries for the given player.
     *
     * @param uuid The user ID.
     * @return The user file entries.
     */
    public static List<UserFileEntry> getFileEntries(final UUID uuid) {
        if (!entryCache.containsKey(uuid)) {
            final PlayerData data = new PlayerData(uuid);
            if (data.getStorage().isConfigurationSection("userfile")) {
                final ConfigurationSection file = data.getStorage().getConfigurationSection("userfile");
                final SortedMap<Integer, UserFileEntry> entrySource = new TreeMap<>();
                for (final String key : file.getKeys(false)) {
                    entrySource.put(Integer.parseInt(key), UserFileEntry.read(file.getConfigurationSection(key)));
                }

                entryCache.put(uuid, new ArrayList<>(entrySource.values()));
            } else {
                entryCache.put(uuid, new ArrayList<>());
            }
        }
        return Collections.unmodifiableList(entryCache.get(uuid));
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private UserFileIO() {
    }
}
