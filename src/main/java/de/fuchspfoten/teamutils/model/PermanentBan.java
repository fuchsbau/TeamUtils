package de.fuchspfoten.teamutils.model;

import de.fuchspfoten.fuchslib.Messenger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents a permanent ban.
 */
@RequiredArgsConstructor
public class PermanentBan extends UserFileEntry implements Ban {

    /**
     * Registers messages with the messenger API.
     */
    public static void registerMessages() {
        Messenger.register("teamutils.userfileentry.permban");
    }

    /**
     * Reads a permanent ban.
     *
     * @param source The source section.
     * @return The read ban.
     */
    public static PermanentBan readBan(final ConfigurationSection source) {
        return new PermanentBan(source.getString("issuer"), source.getString("reason"));
    }

    /**
     * The issuer of the ban.
     */
    private final String issuer;

    /**
     * The reason of the ban.
     */
    private @Getter final String reason;

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void write(final ConfigurationSection target) {
        target.set("type", "permanentBan");
        target.set("issuer", issuer);
        target.set("reason", reason);
    }

    @Override
    public String toString() {
        final String fmt = Messenger.getFormat("teamutils.userfileentry.permban");
        return String.format(fmt, issuer, reason);
    }
}
