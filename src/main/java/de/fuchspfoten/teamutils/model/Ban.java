package de.fuchspfoten.teamutils.model;

/**
 * Represents a ban.
 */
public interface Ban {

    /**
     * Returns whether this ban is active.
     *
     * @return {@code true} iff the ban is active.
     */
    boolean isActive();

    /**
     * Returns the ban reason.
     *
     * @return The ban reason.
     */
    String getReason();
}
