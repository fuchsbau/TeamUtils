package de.fuchspfoten.teamutils.model;

import de.fuchspfoten.fuchslib.Messenger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents lifting a ban.
 */
@RequiredArgsConstructor
public class Unban extends UserFileEntry implements Ban {

    /**
     * Registers messages with the messenger API.
     */
    public static void registerMessages() {
        Messenger.register("teamutils.userfileentry.unban");
    }

    /**
     * Reads an unban.
     *
     * @param source The source section.
     * @return The read ban.
     */
    public static Unban readUnban(final ConfigurationSection source) {
        return new Unban(source.getString("issuer"), source.getString("reason"));
    }

    /**
     * The issuer of the unban.
     */
    private final String issuer;

    /**
     * The reason of the unban.
     */
    private @Getter final String reason;

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void write(final ConfigurationSection target) {
        target.set("type", "unban");
        target.set("issuer", issuer);
        target.set("reason", reason);
    }

    @Override
    public String toString() {
        final String fmt = Messenger.getFormat("teamutils.userfileentry.unban");
        return String.format(fmt, issuer, reason);
    }
}
