package de.fuchspfoten.teamutils.model;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.util.TimeHelper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents a temporary ban.
 */
@RequiredArgsConstructor
public class TemporaryBan extends UserFileEntry implements Ban {

    /**
     * Registers messages with the messenger API.
     */
    public static void registerMessages() {
        Messenger.register("teamutils.userfileentry.tempban");
        Messenger.register("teamutils.userfileentry.tempbanInactive");
    }

    /**
     * Reads a temporary ban.
     *
     * @param source The source section.
     * @return The read ban.
     */
    public static TemporaryBan readTempBan(final ConfigurationSection source) {
        return new TemporaryBan(source.getString("issuer"), source.getString("reason"),
                source.getLong("deadline"));
    }

    /**
     * The issuer of the ban.
     */
    private final String issuer;

    /**
     * The reason of the ban.
     */
    private @Getter final String reason;

    /**
     * The deadline when the temporary ban ends.
     */
    private final long deadline;

    /**
     * Get the remaining time on this ban.
     *
     * @return The remaining time.
     */
    public long getRemaining() {
        return Math.max(0, deadline - System.currentTimeMillis());
    }

    @Override
    public boolean isActive() {
        return System.currentTimeMillis() < deadline;
    }

    @Override
    public void write(final ConfigurationSection target) {
        target.set("type", "tempBan");
        target.set("issuer", issuer);
        target.set("reason", reason);
        target.set("deadline", deadline);
    }

    @Override
    public String toString() {
        final String fmt;
        if (isActive()) {
            fmt = Messenger.getFormat("teamutils.userfileentry.tempban");
        } else {
            fmt = Messenger.getFormat("teamutils.userfileentry.tempbanInactive");
        }

        final long remaining = Math.max(0, deadline - System.currentTimeMillis());
        return String.format(fmt, issuer, reason, TimeHelper.formatTimeDifference(remaining));
    }
}
