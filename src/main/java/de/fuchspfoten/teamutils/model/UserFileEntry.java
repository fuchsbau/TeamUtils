package de.fuchspfoten.teamutils.model;

import org.bukkit.configuration.ConfigurationSection;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Represents an entry in the user file.
 */
public abstract class UserFileEntry {

    /**
     * The entry creation registry.
     */
    private static final Map<String, Function<ConfigurationSection, UserFileEntry>> registry = new HashMap<>();

    static {
        //noinspection Convert2MethodRef
        registry.put("permanentBan", c -> PermanentBan.readBan(c));
        //noinspection Convert2MethodRef
        registry.put("unban", c -> Unban.readUnban(c));
        //noinspection Convert2MethodRef
        registry.put("tempBan", c -> TemporaryBan.readTempBan(c));
    }

    /**
     * Reads an entry.
     *
     * @param source The source section.
     * @return The read entry.
     */
    public static UserFileEntry read(final ConfigurationSection source) {
        final Function<ConfigurationSection, UserFileEntry> creator =
                registry.get(source.getString("type", "default"));
        if (creator == null) {
            throw new IllegalArgumentException("invalid type attribute " + source.getString("type"));
        }

        return creator.apply(source);
    }

    /**
     * Writes the entry to the given section.
     *
     * @param target The section to write to.
     */
    public abstract void write(ConfigurationSection target);
}
