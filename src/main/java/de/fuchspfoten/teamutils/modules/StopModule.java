package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collection;

/**
 * /stop manages server shutdowns.
 */
public class StopModule implements CommandExecutor {

    /**
     * The parser for command arguments for /stop.
     */
    private final ArgumentParser argumentParserStop;

    /**
     * The currently scheduled tasks.
     */
    private final Collection<Integer> scheduledTasks = new ArrayList<>();

    /**
     * Constructor.
     */
    public StopModule() {
        Messenger.register("teamutils.stop.reason");
        Messenger.register("teamutils.stop.unspecified");
        Messenger.register("teamutils.stop.timeout");
        Messenger.register("teamutils.stop.abort");

        argumentParserStop = new ArgumentParser();
        argumentParserStop.addSwitch('i', "Immediate shutdown");
        argumentParserStop.addSwitch('c', "Cancels all shutdowns");
        argumentParserStop.addArgument('t', "Sets the time to shutdown in seconds.", "secs");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.stop")) {
            sender.sendMessage("Missing permission: teamutils.stop");
            return true;
        }

        // Syntax check.
        args = argumentParserStop.parse(args);
        if (argumentParserStop.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /stop <message>");
            argumentParserStop.showHelp(sender);
            return true;
        }

        // Cancel all previous restarts.
        scheduledTasks.forEach(Bukkit.getScheduler()::cancelTask);

        if (argumentParserStop.hasArgument('c')) {
            // For cancelling, just don't add a new restart.
            Bukkit.getOnlinePlayers().forEach(p -> Messenger.send(p, "teamutils.stop.abort"));
            return true;
        }

        // Compose message.
        final StringBuilder builder = new StringBuilder();
        for (final String arg : args) {
            builder.append(arg).append(' ');
        }
        final String message = builder.length() > 0 ? builder.toString().trim()
                : Messenger.getFormat("teamutils.stop.unspecified");

        // Is it an immediate shutdown?
        final boolean immediate = argumentParserStop.hasArgument('i');

        // Determine the time.
        int defaultSeconds;
        try {
            if (argumentParserStop.hasArgument('t')) {
                defaultSeconds = Integer.parseInt(argumentParserStop.getArgument('t'));
            } else {
                defaultSeconds = 30;
            }
        } catch (final NumberFormatException ex) {
            defaultSeconds = 30;
        }
        final int seconds = immediate ? 0 : defaultSeconds;

        // Send the initial message.
        Bukkit.getOnlinePlayers().forEach(p -> Messenger.send(p, "teamutils.stop.reason", message));

        // Send the timeout messages.
        for (int i = 0; i < seconds; i += 5) {
            final int secondsElapsed = i;
            final Runnable send = () -> Bukkit.getOnlinePlayers().forEach(p -> {
                Messenger.send(p, "teamutils.stop.timeout", seconds - secondsElapsed);
                p.playSound(p.getLocation(), Sound.ENTITY_GHAST_SCREAM, 1.0f, 1.0f);
            });
            if (i == 0) {
                send.run();
            } else {
                scheduledTasks.add(TeamUtilsPlugin.scheduleTask(send, i * 20L));
            }
        }

        // Schedule the shutdown.
        if (seconds == 0) {
            // Kick all players.
            Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer(message));
            Bukkit.shutdown();
        } else {
            scheduledTasks.add(TeamUtilsPlugin.scheduleTask(() -> {
                Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer(message));
                Bukkit.shutdown();
            }, seconds * 20L));
        }

        return true;
    }
}
