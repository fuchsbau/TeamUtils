package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.List;

/**
 * Collects user names and associates them with UUIDs.
 */
public class UsernameCollectorModule implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(final PlayerLoginEvent event) {
        final PlayerData pd = new PlayerData(event.getPlayer());
        pd.getStorage().set("stats.currentName", event.getPlayer().getName());

        final List<String> usernames = pd.getStorage().getStringList("stats.usernames");
        if (usernames.contains(event.getPlayer().getName())) {
            TeamUtilsPlugin.getSelf().getLogger().info("Username already known for UUID.");
        } else {
            usernames.add(event.getPlayer().getName());
            pd.getStorage().set("stats.usernames", usernames);
            TeamUtilsPlugin.getSelf().getLogger().info("Username added to UUID.");
        }
    }
}
