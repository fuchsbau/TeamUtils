package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.teamutils.UserFileIO;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * /whois &lt;user&gt; retrieves information about a player.
 */
public class WhoisModule implements CommandExecutor {

    /**
     * The parser for command arguments for /whois.
     */
    private final ArgumentParser argumentParserWhois;

    /**
     * Constructor.
     */
    public WhoisModule() {
        Messenger.register("teamutils.whois.headerFormat");
        Messenger.register("teamutils.whois.uuidFormat");
        Messenger.register("teamutils.whois.positionFormat");
        Messenger.register("teamutils.whois.gamemodeFormat");
        Messenger.register("teamutils.whois.ipFormat");
        Messenger.register("teamutils.whois.firstSeenFormat");
        Messenger.register("teamutils.whois.lastSeenFormat");
        Messenger.register("teamutils.whois.targetOnline");
        Messenger.register("teamutils.whois.targetOffline");
        Messenger.register("teamutils.whois.knownNames");
        Messenger.register("teamutils.whois.possibleAliases");
        Messenger.register("teamutils.whois.numFileEntries");

        argumentParserWhois = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.whois")) {
            sender.sendMessage("Missing permission: teamutils.whois");
            return true;
        }

        // Syntax check.
        args = argumentParserWhois.parse(args);
        if (args.length != 1 || argumentParserWhois.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /whois <user>");
            argumentParserWhois.showHelp(sender);
            return true;
        }

        // Fetch the target.
        final OfflinePlayer targetOffline = Bukkit.getOfflinePlayer(UUIDLookup.lookup(args[0]));
        final PlayerData pd = new PlayerData(targetOffline);

        // General information.
        Messenger.send(sender, "teamutils.whois.headerFormat", targetOffline.getName());
        Messenger.send(sender, "teamutils.whois.uuidFormat", targetOffline.getUniqueId().toString());

        // Timestamp information.
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String firstSeen = format.format(new Date(targetOffline.getFirstPlayed()));
        final String lastSeen = format.format(new Date(targetOffline.getLastPlayed()));
        Messenger.send(sender, "teamutils.whois.firstSeenFormat", firstSeen);
        Messenger.send(sender, "teamutils.whois.lastSeenFormat", lastSeen);

        // User name and taint information.
        final List<String> usernames = pd.getStorage().getStringList("stats.usernames");
        Messenger.send(sender, "teamutils.whois.knownNames", String.join(", ", usernames));
        final List<String> aliasNames = pd.getStorage().getStringList("stats.taint").stream()
                .map(UUID::fromString)
                .map(PlayerData::new)
                .map(PlayerData::getStorage)
                .map(s -> s.getString("stats.currentName"))
                .collect(Collectors.toList());
        Messenger.send(sender, "teamutils.whois.possibleAliases", String.join(", ", aliasNames));

        if (targetOffline.isOnline()) {
            final Player target = targetOffline.getPlayer();

            // Notify about online status.
            Messenger.send(sender, "teamutils.whois.targetOnline");

            // Ingame information.
            final Location pos = target.getLocation();
            Messenger.send(sender, "teamutils.whois.positionFormat", pos.getBlockX(), pos.getBlockY(),
                    pos.getBlockZ(), pos.getWorld().getName());
            Messenger.send(sender, "teamutils.whois.gamemodeFormat", target.getGameMode().name());

            // Connection information.
            if (sender.hasPermission("teamutils.whois.ip")) {
                Messenger.send(sender, "teamutils.whois.ipFormat", target.getAddress().getAddress().toString());
            }
        } else {
            Messenger.send(sender, "teamutils.whois.targetOffline");
        }

        // File information.
        Messenger.send(sender, "teamutils.whois.numFileEntries",
                UserFileIO.getFileEntries(targetOffline.getUniqueId()).size());

        return true;
    }
}
