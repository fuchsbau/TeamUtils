package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;

/**
 * /vanish [-i] vanishes the player.
 */
public class VanishModule implements CommandExecutor, Listener {

    /**
     * The vanished users.
     */
    private final Collection<UUID> vanished = new HashSet<>();

    /**
     * The vanished users for which interaction is allowed.
     */
    private final Collection<UUID> allowInteract = new HashSet<>();

    /**
     * The IDs of players currently viewing inventories.
     */
    private final Collection<UUID> silentOpenInventory = new HashSet<>();

    /**
     * The parser for command arguments for /vanish.
     */
    private final ArgumentParser argumentParserVanish;

    /**
     * Constructor.
     */
    public VanishModule() {
        Messenger.register("teamutils.vanish.vanishFormat");
        Messenger.register("teamutils.vanish.appearFormat");
        Messenger.register("teamutils.vanish.notifyVanishFormat");
        Messenger.register("teamutils.vanish.notifyAppearFormat");
        Messenger.register("teamutils.vanish.statusInvisible");
        Messenger.register("teamutils.vanish.statusInteractAllowed");
        Messenger.register("teamutils.vanish.statusInteractBlocked");
        Messenger.register("teamutils.vanish.statusVisible");

        // /vanish parser.
        argumentParserVanish = new ArgumentParser();
        argumentParserVanish.addSwitch('d', "Disallow interaction with the world");
        argumentParserVanish.addSwitch('i', "Allow interaction with the world");
        argumentParserVanish.addSwitch('s', "Shows your vanish status");

        Bukkit.getPluginCommand("vanish").setExecutor(this);
    }

    /**
     * Obtains the IDs vanished.
     *
     * @return The vanished IDs.
     */
    public Collection<UUID> getVanishedIDs() {
        return Collections.unmodifiableCollection(vanished);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityTargetLivingEntityEvent(final EntityTargetLivingEntityEvent event) {
        if (event.getTarget() == null) {
            return;
        }

        if (!mayInteract(event.getTarget().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (isVanished(event.getPlayer().getUniqueId())) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                // Whitelisted right click actions with special handling.
                final BlockState state = event.getClickedBlock().getState();
                if (state instanceof Container) {
                    final Inventory toCopy = ((InventoryHolder) state).getInventory();
                    final Inventory clone;
                    if (toCopy.getType() == InventoryType.CHEST) {
                        clone = Bukkit.createInventory(null, 54, toCopy.getTitle());
                    } else {
                        clone = Bukkit.createInventory(null, toCopy.getType(), toCopy.getTitle());
                    }
                    clone.setStorageContents(toCopy.getStorageContents());
                    event.getPlayer().openInventory(clone);
                    silentOpenInventory.add(event.getPlayer().getUniqueId());

                    // Cancel even if allowed to interact.
                    event.setCancelled(true);
                    return;
                }
            }

            if (!mayInteract(event.getPlayer().getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (!event.getPlayer().hasPermission("teamutils.vanish")) {
            for (final Player player : Bukkit.getOnlinePlayers()) {
                if (isVanished(player.getUniqueId())) {
                    event.getPlayer().hidePlayer(TeamUtilsPlugin.getSelf(), player);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPickupArrow(final PlayerPickupArrowEvent event) {
        if (!mayInteract(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamage(final EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (!mayInteract(event.getEntity().getUniqueId())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPickupItem(final EntityPickupItemEvent event) {
        if (!mayInteract(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!mayInteract(event.getDamager().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(final PlayerQuitEvent event) {
        vanished.remove(event.getPlayer().getUniqueId());
        allowInteract.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryClick(final InventoryClickEvent event) {
        if (silentOpenInventory.contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (silentOpenInventory.contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        silentOpenInventory.remove(event.getPlayer().getUniqueId());
    }

    /**
     * Checks if the given ID is allowed to interact, i.e. the ID is not vanished or exempt from the interaction
     * prohibition.
     *
     * @param id The UUID of the entity in question.
     * @return {@code true} if the entity designated by the ID is allowed to interact.
     */
    private boolean mayInteract(final UUID id) {
        return !isVanished(id) || allowInteract.contains(id);
    }

    /**
     * Checks if the given ID is vanished.
     *
     * @param id The UUID of the entity in question.
     * @return {@code true} if the entity designated by the ID is vanished.
     */
    private boolean isVanished(final UUID id) {
        return vanished.contains(id);
    }

    /**
     * Shows the current interaction status of the entity to itself.
     *
     * @param entity The entity.
     */
    private void showInteractStatus(final Entity entity) {
        if (mayInteract(entity.getUniqueId())) {
            Messenger.send(entity, "teamutils.vanish.statusInteractAllowed");
        } else {
            Messenger.send(entity, "teamutils.vanish.statusInteractBlocked");
        }
    }

    /**
     * Applies any vanish change with regards to visibility and notification.
     *
     * @param changed The player whose vanish status changed.
     */
    private void applyVanishChange(final Player changed) {
        final boolean nowVanished = isVanished(changed.getUniqueId());
        final String selfFormat = nowVanished ? "teamutils.vanish.vanishFormat" : "teamutils.vanish.appearFormat";
        final String otherFormat = nowVanished ? "teamutils.vanish.notifyVanishFormat"
                : "teamutils.vanish.notifyAppearFormat";

        for (final Player notified : Bukkit.getOnlinePlayers()) {
            if (notified.hasPermission("teamutils.vanish")) {
                if (notified == changed) {
                    Messenger.send(notified, selfFormat);
                    showInteractStatus(notified);
                } else {
                    Messenger.send(notified, otherFormat, changed.getName());
                }
            } else {
                if (nowVanished) {
                    notified.hidePlayer(TeamUtilsPlugin.getSelf(), changed);
                } else {
                    notified.showPlayer(TeamUtilsPlugin.getSelf(), changed);
                }
            }
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.vanish") || !(sender instanceof Player)) {
            sender.sendMessage("Missing permission: teamutils.vanish");
            return true;
        }
        args = argumentParserVanish.parse(args);

        // Syntax check.
        if (argumentParserVanish.hasArgument('h') || args.length != 0) {
            Messenger.send(sender, "commandHelp", "Syntax: /vanish");
            argumentParserVanish.showHelp(sender);
            return true;
        }
        final Player issuer = (Player) sender;

        // Status check (if requested).
        if (argumentParserVanish.hasArgument('s')) {
            if (isVanished(issuer.getUniqueId())) {
                Messenger.send(issuer, "teamutils.vanish.statusInvisible");
                showInteractStatus(issuer);
            } else {
                Messenger.send(issuer, "teamutils.vanish.statusVisible");
            }
            return true;
        }

        // Vanish status change.
        if (isVanished(issuer.getUniqueId())) {
            if (argumentParserVanish.hasArgument('d')) {
                allowInteract.remove(issuer.getUniqueId());
                showInteractStatus(issuer);
            } else if (argumentParserVanish.hasArgument('i')) {
                allowInteract.add(issuer.getUniqueId());
                showInteractStatus(issuer);
            } else {
                // No flags: appear.
                vanished.remove(issuer.getUniqueId());
                allowInteract.remove(issuer.getUniqueId());
                applyVanishChange(issuer);
            }
        } else {
            if (argumentParserVanish.hasArgument('i')) {
                // Only -i counts: -d is implicit.
                allowInteract.add(issuer.getUniqueId());
            }
            vanished.add(issuer.getUniqueId());
            applyVanishChange(issuer);
        }

        return true;
    }
}
