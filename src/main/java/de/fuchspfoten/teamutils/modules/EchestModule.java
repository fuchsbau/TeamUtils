package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * /echest &lt;user&gt; opens the ender chest of a player.
 */
public class EchestModule implements CommandExecutor, Listener {

    /**
     * The parser for command arguments for /echest.
     */
    private final ArgumentParser argumentParserEchest;

    /**
     * The IDs of players currently viewing enderchests.
     */
    private final Collection<UUID> openEnderchest = new HashSet<>();

    /**
     * Constructor.
     */
    public EchestModule() {
        argumentParserEchest = new ArgumentParser();
        Bukkit.getPluginCommand("echest").setExecutor(this);
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (openEnderchest.contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(!event.getWhoClicked().hasPermission("teamutils.echest.modify"));
        }
    }

    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (openEnderchest.contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(!event.getWhoClicked().hasPermission("teamutils.echest.modify"));
        }
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        openEnderchest.remove(event.getPlayer().getUniqueId());
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Permission check.
        if (!sender.hasPermission("teamutils.echest")) {
            sender.sendMessage("Missing permission: teamutils.echest");
            return true;
        }

        // Syntax check.
        args = argumentParserEchest.parse(args);
        if (args.length != 1 || argumentParserEchest.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /echest <user>");
            argumentParserEchest.showHelp(sender);
            return true;
        }

        // Get the target.
        final Player target = PlayerHelper.requireVisiblePlayerByName(player, args[0]);
        if (target == null) {
            return true;
        }

        // Open the ender chest.
        openEnderchest.add(player.getUniqueId());
        player.openInventory(target.getEnderChest());
        return true;
    }
}
