package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Implements the team chat, used with the prefix #.
 */
public class TeamChatModule implements Listener {

    /**
     * Constructor.
     */
    public TeamChatModule() {
        Messenger.register("teamutils.teamchat.format");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerChat(final AsyncPlayerChatEvent event) {
        if (event.getMessage().startsWith("#") && event.getPlayer().hasPermission("teamutils.teamchat")) {
            final String message = event.getMessage().substring(1).trim();
            if (!message.isEmpty()) {
                Bukkit.getOnlinePlayers().stream()
                        .filter(x -> x.hasPermission("teamutils.teamchat"))
                        .forEach(p -> Messenger.send(p, "teamutils.teamchat.format", event.getPlayer().getName(),
                                message));
                Messenger.send(Bukkit.getConsoleSender(), "teamutils.teamchat.format",
                        event.getPlayer().getName(), message);
                event.setCancelled(true);
            }
        }
    }
}
