package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Bans mobile chat clients.
 */
public class ChatClientBanModule implements Listener {

    /**
     * The pattern that recognizes chat clients.
     */
    private static final Pattern RECOGNITION_PATTERN =
            Pattern.compile(".*\\b(?i)(?:connected|verbunden|android|minechat|ios)\\b.*");

    /**
     * The players that are still monitored for chat-client-like ad messages.
     */
    private final Set<UUID> disgracePeriodSet = new HashSet<>();

    /**
     * Constructor.
     */
    public ChatClientBanModule() {
        Messenger.register("teamutils.chatClient.kickReason");
        Messenger.register("teamutils.chatClient.notifyKick");
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        disgracePeriodSet.add(event.getPlayer().getUniqueId());
        TeamUtilsPlugin.scheduleTask(() -> disgracePeriodSet.remove(event.getPlayer().getUniqueId()), 5 * 20L);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAsyncPlayerChatEvent(final AsyncPlayerChatEvent event) {
        // Only kick in the disgrace period as to prevent accidental conversation kicks.
        synchronized (disgracePeriodSet) {
            if (!disgracePeriodSet.contains(event.getPlayer().getUniqueId())) {
                return;
            }
        }

        // Check the recognition pattern.
        final boolean recognizedPattern = RECOGNITION_PATTERN.matcher(event.getMessage()).matches();
        if (recognizedPattern) {
            event.setCancelled(true);

            // Kick and notify.
            TeamUtilsPlugin.scheduleTask(() -> {
                event.getPlayer().kickPlayer(Messenger.getFormat("teamutils.chatClient.kickReason"));
                Bukkit.getOnlinePlayers().stream()
                        .filter(x -> x.hasPermission("teamutils.chatClient.notify"))
                        .forEach(x -> Messenger.send(x, "teamutils.chatClient.notifyKick",
                                event.getPlayer().getName()));
            }, 1L);
        }
    }
}
