package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.data.PlayerData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Associates users with possible alternative accounts.
 */
public class TaintCollectorModule implements Listener {

    private final Map<String, Set<UUID>> taintMap = new HashMap<>();

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final String ipAddress = event.getPlayer().getAddress().getAddress().toString();
        final Set<UUID> alternatives = taintMap.computeIfAbsent(ipAddress, i -> new HashSet<>());

        if (!alternatives.contains(event.getPlayer().getUniqueId())) {
            alternatives.add(event.getPlayer().getUniqueId());

            // Update taint tracking.
            if (alternatives.size() > 1) {
                for (final UUID alternative : alternatives) {
                    final PlayerData taintData = new PlayerData(alternative);
                    final List<String> knownTaint = taintData.getStorage().getStringList("stats.taint");

                    boolean dirty = false;
                    for (final UUID taintAccount : alternatives) {
                        if (taintAccount == alternative) {
                            continue;
                        }

                        if (!knownTaint.contains(taintAccount.toString())) {
                            knownTaint.add(taintAccount.toString());
                            dirty = true;
                        }
                    }

                    if (dirty) {
                        taintData.getStorage().set("stats.taint", knownTaint);
                    }
                }
            }
        }
    }
}
