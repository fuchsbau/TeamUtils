package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * /gms [player] enters survival mode.
 * /gma [player] enters adventure mode.
 * /gmc [player] enters creative mode.
 * /gmsp [player] enters spectator mode.
 */
public class GamemodeModule implements CommandExecutor, Listener {

    /**
     * The parser for command arguments for /gm*.
     */
    private final ArgumentParser argumentParserGm;

    /**
     * Constructor.
     */
    public GamemodeModule() {
        Messenger.register("teamutils.gamemode.changeSelf");
        Messenger.register("teamutils.gamemode.changeOther");

        argumentParserGm = new ArgumentParser();

        Bukkit.getPluginCommand("gmc").setExecutor(this);
        Bukkit.getPluginCommand("gma").setExecutor(this);
        Bukkit.getPluginCommand("gms").setExecutor(this);
        Bukkit.getPluginCommand("gmsp").setExecutor(this);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.gamemode")) {
            sender.sendMessage("Missing permission: teamutils.gamemode");
            return true;
        }

        // Syntax check.
        args = argumentParserGm.parse(args);
        if (args.length > 1 || argumentParserGm.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /gm.. [player]");
            argumentParserGm.showHelp(sender);
            return true;
        }

        // Determine target.
        final Player target;
        if (args.length == 1 && sender.hasPermission("teamutils.gamemode.other")) {
            target = Bukkit.getPlayer(args[0]);
            if (target == null || !target.isOnline()) {
                sender.sendMessage("target not found");
                return true;
            }
        } else if (sender instanceof Player) {
            target = (Player) sender;
        } else {
            sender.sendMessage("Player-only command!");
            return true;
        }

        // Determine target mode.
        final GameMode targetMode;
        switch (command.getName()) {
            case "gmc":
                targetMode = GameMode.CREATIVE;
                break;
            case "gma":
                targetMode = GameMode.ADVENTURE;
                break;
            case "gms":
                targetMode = GameMode.SURVIVAL;
                break;
            case "gmsp":
                targetMode = GameMode.SPECTATOR;
                break;
            default:
                targetMode = GameMode.SURVIVAL;
                break;
        }

        // Change the game mode.
        target.setGameMode(targetMode);
        Messenger.send(target, "teamutils.gamemode.changeSelf", targetMode.name().toLowerCase());
        if (target != sender) {
            Messenger.send(sender, "teamutils.gamemode.changeOther", target.getName(),
                    targetMode.name().toLowerCase());
        }

        return true;
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            if (!event.getPlayer().hasPermission("teamutils.gamemode.keep")) {
                event.getPlayer().setGameMode(GameMode.SURVIVAL);
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        if (event.getFrom().getWorld() == event.getTo().getWorld()) {
            return;  // No world change.
        }

        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            if (!event.getPlayer().hasPermission("teamutils.gamemode.keep")) {
                event.getPlayer().setGameMode(GameMode.SURVIVAL);
                Messenger.send(event.getPlayer(), "teamutils.gamemode.changeSelf", "survival");
            }
        }
    }
}
