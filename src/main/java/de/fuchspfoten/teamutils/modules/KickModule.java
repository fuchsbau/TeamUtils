package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import java.util.HashMap;
import java.util.Map;

/**
 * /kick [-t] &lt;user&gt; [message] temporarily bans the given user for the given amount of time.
 * /unkick &lt;user&gt; revokes any kicks for the given user.
 */
public class KickModule implements CommandExecutor, Listener {

    /**
     * The default reason.
     */
    private final String defaultKickReason;

    /**
     * The storage of banned names (no UUIDs: not persistent).
     */
    private final Map<String, String> temporaryBans = new HashMap<>();

    /**
     * The parser for command arguments for /kick.
     */
    private final ArgumentParser argumentParserKick;

    /**
     * The parser for command arguments for /unkick.
     */
    private final ArgumentParser argumentParserUnkick;

    /**
     * Constructor.
     *
     * @param config The global configuration.
     */
    public KickModule(final ConfigurationSection config) {
        Messenger.register("teamutils.kick.joinMessage");
        Messenger.register("teamutils.kick.notifyKick");
        Messenger.register("teamutils.kick.notifyKickNoTimeout");
        Messenger.register("teamutils.kick.notifyUnkick");
        defaultKickReason = config.getString("modules.kick.defaultReason");

        // /kick parser.
        argumentParserKick = new ArgumentParser();
        argumentParserKick.addSwitch('t', "Do not add a timeout to the kick");

        // /unkick parser.
        argumentParserUnkick = new ArgumentParser();

        Bukkit.getPluginCommand("kick").setExecutor(this);
        Bukkit.getPluginCommand("unkick").setExecutor(this);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(final PlayerLoginEvent event) {
        if (temporaryBans.containsKey(event.getPlayer().getName())) {
            final String message = temporaryBans.get(event.getPlayer().getName());
            event.setResult(Result.KICK_BANNED);
            final String fmt = Messenger.getFormat("teamutils.kick.joinMessage");
            event.setKickMessage(String.format(fmt, message));
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.kick")) {
            sender.sendMessage("Missing permission: teamutils.kick");
            return true;
        }

        if (label.equalsIgnoreCase("kick")) {
            args = argumentParserKick.parse(args);

            // Syntax check.
            if (argumentParserKick.hasArgument('h') || args.length < 1) {
                Messenger.send(sender, "commandHelp", "Syntax: /kick <user> [message]");
                argumentParserKick.showHelp(sender);
                return true;
            }

            // Get the target.
            final Player target = Bukkit.getPlayer(args[0]);
            if (target == null || !target.isOnline()) {
                sender.sendMessage("Target not online.");
                return true;
            }

            // Compose message.
            final String message;
            if (args.length > 1) {
                final StringBuilder builder = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    final String arg = args[i];
                    builder.append(arg).append(' ');
                }
                message = builder.toString().trim();
            } else {
                message = defaultKickReason;
            }

            // Remove the target.
            final String fmt = Messenger.getFormat("teamutils.kick.joinMessage");
            target.kickPlayer(String.format(fmt, message));

            // Timeout, if required.
            if (argumentParserKick.hasArgument('t')) {
                // Broadcast the action.
                Bukkit.getOnlinePlayers().stream()
                        .filter(x -> x.hasPermission("teamutils.kick"))
                        .forEach(x -> Messenger.send(x, "teamutils.kick.notifyKickNoTimeout", target.getName(),
                                sender.getName(), message));
            } else {
                temporaryBans.put(target.getName(), message);
                TeamUtilsPlugin.scheduleTask(() -> temporaryBans.remove(target.getName()), 20L * 60 * 5);  // 5 min

                // Broadcast the action.
                Bukkit.getOnlinePlayers().stream()
                        .filter(x -> x.hasPermission("teamutils.kick"))
                        .forEach(x -> Messenger.send(x, "teamutils.kick.notifyKick", target.getName(),
                                sender.getName(), message));
            }

            return true;
        }
        if (label.equalsIgnoreCase("unkick")) {
            args = argumentParserUnkick.parse(args);

            // Syntax check.
            if (argumentParserUnkick.hasArgument('h') || args.length < 1) {
                Messenger.send(sender, "commandHelp", "Syntax: /unkick <user>");
                argumentParserUnkick.showHelp(sender);
                return true;
            }

            // Remove the kick.
            temporaryBans.remove(args[0]);

            // Broadcast the action.
            final String target = args[0];
            Bukkit.getOnlinePlayers().stream()
                    .filter(x -> x.hasPermission("teamutils.kick"))
                    .forEach(x -> Messenger.send(x, "teamutils.kick.notifyUnkick", target, sender.getName()));
            return true;
        }
        return false;
    }
}
