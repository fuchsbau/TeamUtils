package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.Pagination;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import net.eq2online.permissions.ReplicatedPermissionsContainer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Blocks mods that are not wanted and can be blocked.
 */
public class ModBlockerModule implements CommandExecutor, Listener, PluginMessageListener {

    /**
     * The channel name for the ClientPermission protocol.
     */
    private static final String CLIENTPERMISSION_CHANNEL = "PERMISSIONSREPL";

    /**
     * The parser for command arguments for /mods.
     */
    private final ArgumentParser argumentParserMods;

    /**
     * The channels known to the server.
     */
    private final Map<String, Set<UUID>> knownChannels = new HashMap<>();

    /**
     * The mods known to the server.
     */
    private final Map<String, Set<UUID>> knownMods = new HashMap<>();

    /**
     * The mod permissions known to the server.
     */
    private final Map<String, Set<String>> knownPermissions = new HashMap<>();

    /**
     * Constructor.
     */
    public ModBlockerModule() {
        Bukkit.getMessenger().registerIncomingPluginChannel(TeamUtilsPlugin.getSelf(), CLIENTPERMISSION_CHANNEL,
                this);
        Bukkit.getMessenger().registerOutgoingPluginChannel(TeamUtilsPlugin.getSelf(), CLIENTPERMISSION_CHANNEL);

        argumentParserMods = new ArgumentParser();
        argumentParserMods.addSwitch('c', "Lists all known channels");
        argumentParserMods.addSwitch('m', "Lists all known mods");
        argumentParserMods.addSwitch('u', "Lists known users of objects alongside the objects");
        argumentParserMods.addSwitch('p', "Lists all known permissions");
        argumentParserMods.addArgument('s', "Searches for the given user", "user");
        Bukkit.getPluginCommand("mods").setExecutor(this);
    }

    /**
     * Lists the elements of the given registry into the given data set.
     *
     * @param data         The data set.
     * @param includeUsers Whether or not to include users.
     * @param registry     The registry.
     */
    private static void listRegistry(final Collection<String> data, final boolean includeUsers,
                                     final Map<String, Set<UUID>> registry) {
        for (final Entry<String, Set<UUID>> registryEntry : registry.entrySet()) {
            if (includeUsers) {
                final SortedSet<String> names = registryEntry.getValue().stream()
                        .map(Bukkit::getOfflinePlayer)
                        .map(OfflinePlayer::getName)
                        .collect(Collectors.toCollection(TreeSet::new));
                data.add(registryEntry.getKey() + " - " + String.join(", ", names));
            } else {
                data.add(registryEntry.getKey());
            }
        }
    }

    @EventHandler
    public void onPlayerRegisterChannel(final PlayerRegisterChannelEvent event) {
        knownChannels.computeIfAbsent(event.getChannel(), n -> new HashSet<>()).add(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(final PlayerJoinEvent event) {
        // Blocker for BetterPVP protocol.
        event.getPlayer().sendMessage("§c §r§5 §r§1 §r§f §r§0 ");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.mods")) {
            sender.sendMessage("Missing permission: teamutils.mods");
            return true;
        }

        final String[] oldArgs = Arrays.copyOfRange(args, 0, Math.max(0, args.length - 1));
        args = argumentParserMods.parse(args);

        // Syntax check.
        if (argumentParserMods.hasArgument('h') || args.length < 1) {
            Messenger.send(sender, "commandHelp", "Syntax: /mods <page>");
            argumentParserMods.showHelp(sender);
            return true;
        }

        // Determine the page.
        int page;
        try {
            page = Integer.parseInt(args[0]);
            if (page < 1) {
                page = 1;
            }
        } catch (final NumberFormatException ex) {
            page = 1;
        }

        // Create the data storage.
        final int perPage = 10;
        final SortedSet<String> data = new TreeSet<>();

        if (argumentParserMods.hasArgument('c')) {
            // List channels.
            listRegistry(data, argumentParserMods.hasArgument('u'), knownChannels);
        } else if (argumentParserMods.hasArgument('m')) {
            // List mods.
            listRegistry(data, argumentParserMods.hasArgument('u'), knownMods);
        } else if (argumentParserMods.hasArgument('s')) {
            final UUID who = UUIDLookup.lookupNonBlocking(argumentParserMods.getArgument('s'));
            if (who != null) {
                knownChannels.entrySet().stream().filter(e -> e.getValue().contains(who)).map(Entry::getKey)
                        .forEach(x -> data.add("CHANNEL: " + x));
                knownMods.entrySet().stream().filter(e -> e.getValue().contains(who)).map(Entry::getKey)
                        .forEach(x -> data.add("MOD: " + x));
            }
        } else if (argumentParserMods.hasArgument('p')) {
            knownPermissions.entrySet().stream()
                    .map(e -> e.getValue().stream()
                            .map(p -> e.getKey() + ':' + p)
                            .collect(Collectors.toList()))
                    .forEach(data::addAll);
        }

        // Show the data.
        final int pages = (data.size() % perPage == 0) ? data.size() / perPage : data.size() / perPage + 1;
        int skip = (page - 1) * perPage;
        int keep = perPage;
        Pagination.showPagination(sender, page, pages, "/mods " + String.join(" ", oldArgs) + ' ');
        for (final String entry : data) {
            if (skip-- > 0) {
                continue;
            }
            if (keep-- == 0) {
                break;
            }
            sender.sendMessage(entry);
        }
        Pagination.showPagination(sender, page, pages, "/mods " + String.join(" ", oldArgs) + ' ');

        return true;
    }

    @Override
    public void onPluginMessageReceived(final String channel, final Player player, final byte[] message) {
        if (channel.equals(CLIENTPERMISSION_CHANNEL)) {
            try {
                final ReplicatedPermissionsContainer rpc = ReplicatedPermissionsContainer.fromBytes(message);
                if (rpc != null) {
                    rpc.sanitise();
                    knownMods.computeIfAbsent(rpc.modName, n -> new HashSet<>()).add(player.getUniqueId());
                    knownPermissions.computeIfAbsent(rpc.modName, n -> new HashSet<>()).addAll(rpc.permissions);

                    // Add "-" to all permissions.
                    // TODO: do we want to whitelist some?
                    final List<String> replyPermissions = rpc.permissions.stream()
                            .map(x -> '-' + x)
                            .collect(Collectors.toList());
                    final ReplicatedPermissionsContainer reply =
                            new ReplicatedPermissionsContainer(rpc.modName, rpc.modVersion, replyPermissions);
                    final byte[] replyMessage = reply.getBytes();
                    player.sendPluginMessage(TeamUtilsPlugin.getSelf(), CLIENTPERMISSION_CHANNEL, replyMessage);
                } else {
                    TeamUtilsPlugin.getSelf().getLogger().warning("Unreadable message on channel " + channel + " from "
                            + player.getName());
                }
            } catch (final RuntimeException ignored) {
                TeamUtilsPlugin.getSelf().getLogger().warning("Unreadable message on channel " + channel + " from "
                        + player.getName());
            }
        }
    }
}
