package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

/**
 * /op grants temporary server operator status.
 */
public class OpModule implements CommandExecutor, Listener {

    /**
     * The parser for command arguments for /op.
     */
    private final ArgumentParser argumentParserOp;

    /**
     * Constructor.
     */
    public OpModule() {
        Messenger.register("teamutils.op.notifyEnter");
        Messenger.register("teamutils.op.notifyExit");

        argumentParserOp = new ArgumentParser();
        Bukkit.getPluginCommand("op").setExecutor(this);
    }

    /**
     * De-ops all online players.
     */
    public void deopAll() {
        for (final Player online : Bukkit.getOnlinePlayers()) {
            if (online.isOp()) {
                online.setOp(false);
                TeamUtilsPlugin.getSelf().getLogger().info("Deopped " + online.getName());
            }
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // Permission check.
        if (!sender.hasPermission("dangerzone.op")) {
            sender.sendMessage("Missing permission for /op. This incident will be reported.");
            Bukkit.broadcast(sender.getName() + " attempted to /op and was denied access.",
                    "dangerzone.notify");
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage("Player-only command!");
            return true;
        }

        // Syntax check.
        /*args = */
        argumentParserOp.parse(args);
        if (argumentParserOp.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /op");
            argumentParserOp.showHelp(sender);
            return true;
        }

        // Enable temporary OP
        Bukkit.getOnlinePlayers().stream()
                .filter(p -> p.hasPermission("dangerzone.notify"))
                .forEach(p -> Messenger.send(p, "teamutils.op.notifyEnter", sender.getName()));
        sender.setOp(true);
        Bukkit.getConsoleSender().sendMessage("§4§lOpped " + sender.getName() + " on request.");

        final UUID senderID = ((Entity) sender).getUniqueId();
        TeamUtilsPlugin.scheduleTask(() -> {
            final OfflinePlayer target = Bukkit.getOfflinePlayer(senderID);
            if (!target.isOp()) {
                return;
            }
            Bukkit.getOnlinePlayers().stream()
                    .filter(p -> p.hasPermission("dangerzone.notify"))
                    .forEach(p -> Messenger.send(p, "teamutils.op.notifyExit", target.getName()));
            target.setOp(false);
        }, 600 * 20L);

        return true;
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        if (event.getPlayer().isOp()) {
            event.getPlayer().setOp(false);
            Bukkit.getOnlinePlayers().stream()
                    .filter(p -> p.hasPermission("dangerzone.notify"))
                    .forEach(p -> Messenger.send(p, "teamutils.op.notifyExit", event.getPlayer().getName()));
        }
    }
}
