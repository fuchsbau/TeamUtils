package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.PlayerHelper;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * /invsee &lt;user&gt; opens the inventory of a player.
 */
public class InvseeModule implements CommandExecutor, Listener {

    /**
     * The parser for command arguments for /invsee.
     */
    private final ArgumentParser argumentParserInvsee;

    /**
     * The IDs of players currently viewing inventories.
     */
    private final Collection<UUID> openInventory = new HashSet<>();

    /**
     * Constructor.
     */
    public InvseeModule() {
        argumentParserInvsee = new ArgumentParser();
        Bukkit.getPluginCommand("invsee").setExecutor(this);
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (openInventory.contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(!event.getWhoClicked().hasPermission("teamutils.invsee.modify"));
        }
    }

    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (openInventory.contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(!event.getWhoClicked().hasPermission("teamutils.invsee.modify"));
        }
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        openInventory.remove(event.getPlayer().getUniqueId());
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        final Player player = PlayerHelper.ensurePlayer(sender);
        if (player == null) {
            return true;
        }

        // Permission check.
        if (!sender.hasPermission("teamutils.invsee")) {
            sender.sendMessage("Missing permission: teamutils.invsee");
            return true;
        }

        // Syntax check.
        args = argumentParserInvsee.parse(args);
        if (args.length != 1 || argumentParserInvsee.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /invsee <user>");
            argumentParserInvsee.showHelp(sender);
            return true;
        }

        // Get the target.
        final Player target = PlayerHelper.requireVisiblePlayerByName(player, args[0]);
        if (target == null) {
            return true;
        }

        // Open the inventory.
        openInventory.add(player.getUniqueId());
        player.openInventory(target.getInventory());
        return true;
    }
}
