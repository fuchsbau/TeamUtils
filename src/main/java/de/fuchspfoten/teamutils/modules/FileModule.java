package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.teamutils.UserFileIO;
import de.fuchspfoten.teamutils.model.UserFileEntry;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;
import java.util.UUID;

/**
 * /file &lt;user&gt; retrieves the user file of a player.
 */
public class FileModule implements CommandExecutor {

    /**
     * The parser for command arguments for /file.
     */
    private final ArgumentParser argumentParserFile;

    /**
     * Constructor.
     */
    public FileModule() {
        Messenger.register("teamutils.file.header");

        argumentParserFile = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.file")) {
            sender.sendMessage("Missing permission: teamutils.file");
            return true;
        }

        // Syntax check.
        args = argumentParserFile.parse(args);
        if (args.length != 1 || argumentParserFile.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /file <user>");
            argumentParserFile.showHelp(sender);
            return true;
        }

        // Fetch the target and its user file.
        final UUID target = UUIDLookup.lookup(args[0]);
        final OfflinePlayer targetOffline = Bukkit.getOfflinePlayer(target);
        final List<UserFileEntry> entries = UserFileIO.getFileEntries(target);

        Messenger.send(sender, "teamutils.file.header", targetOffline.getName(), entries.size());
        for (final UserFileEntry entry : entries) {
            sender.sendMessage(entry.toString());
        }

        return true;
    }
}
