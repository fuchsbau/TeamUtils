package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * /raw &lt;message&gt; sends an unformatted message to the chat with format codes applied.
 */
public class RawModule implements CommandExecutor {

    /**
     * The parser for command arguments for /raw.
     */
    private final ArgumentParser argumentParserRaw;

    /**
     * Constructor.
     */
    public RawModule() {
        argumentParserRaw = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.raw")) {
            sender.sendMessage("Missing permission: teamutils.raw");
            return true;
        }

        // Syntax check.
        args = argumentParserRaw.parse(args);
        if (args.length < 1 || argumentParserRaw.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /raw <message>");
            argumentParserRaw.showHelp(sender);
            return true;
        }

        // Compose message.
        final StringBuilder builder = new StringBuilder();
        for (final String arg : args) {
            builder.append(arg).append(' ');
        }
        final String message = builder.toString().trim();

        // Send the message.
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
        return true;
    }
}
