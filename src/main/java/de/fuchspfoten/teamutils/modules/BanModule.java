package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.fuchslib.util.TimeHelper;
import de.fuchspfoten.teamutils.UserFileIO;
import de.fuchspfoten.teamutils.model.Ban;
import de.fuchspfoten.teamutils.model.PermanentBan;
import de.fuchspfoten.teamutils.model.TemporaryBan;
import de.fuchspfoten.teamutils.model.Unban;
import de.fuchspfoten.teamutils.model.UserFileEntry;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * /ban [-u] &lt;user&gt; [message] permanently bans the given user.
 */
public class BanModule implements CommandExecutor, Listener {

    /**
     * The default reason.
     */
    private final String defaultBanReason;

    /**
     * The parser for command arguments for /ban.
     */
    private final ArgumentParser argumentParserBan;

    /**
     * Constructor.
     *
     * @param config The global configuration.
     */
    public BanModule(final ConfigurationSection config) {
        Messenger.register("teamutils.ban.joinMessage");
        Messenger.register("teamutils.ban.joinMessageTemporary");
        Messenger.register("teamutils.ban.notifyBan");
        Messenger.register("teamutils.ban.notifyTempban");
        Messenger.register("teamutils.ban.notifyUnban");
        defaultBanReason = config.getString("modules.ban.defaultReason");

        // /kick parser.
        argumentParserBan = new ArgumentParser();
        argumentParserBan.addSwitch('u', "Unbans the given user");
        argumentParserBan.addArgument('t', "Temporarily bans user", "Time");

        Bukkit.getPluginCommand("ban").setExecutor(this);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(final PlayerLoginEvent event) {
        final List<UserFileEntry> entries = UserFileIO.getFileEntries(event.getPlayer().getUniqueId());
        final List<Ban> bans = entries.stream()
                .filter(e -> e instanceof Ban)
                .map(e -> (Ban) e)
                .collect(Collectors.toList());

        if (!bans.isEmpty()) {
            final Ban lastBan = bans.get(bans.size() - 1);
            if (lastBan.isActive()) {
                event.setResult(Result.KICK_BANNED);
                if (lastBan instanceof TemporaryBan) {
                    final String fmt = Messenger.getFormat("teamutils.ban.joinMessageTemporary");
                    event.setKickMessage(String.format(fmt, lastBan.getReason(),
                            TimeHelper.formatTimeDifference(((TemporaryBan) lastBan).getRemaining())));
                } else {
                    final String fmt = Messenger.getFormat("teamutils.ban.joinMessage");
                    event.setKickMessage(String.format(fmt, lastBan.getReason()));
                }
            }
        }
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.ban")) {
            sender.sendMessage("Missing permission: teamutils.ban");
            return true;
        }

        // Syntax check.
        args = argumentParserBan.parse(args);
        if (argumentParserBan.hasArgument('h') || args.length < 1) {
            Messenger.send(sender, "commandHelp", "Syntax: /ban <user> [message]");
            argumentParserBan.showHelp(sender);
            return true;
        }

        // Get the target.
        final UUID target = UUIDLookup.lookup(args[0]);
        final OfflinePlayer offlineTarget = Bukkit.getOfflinePlayer(target);

        // Compose message.
        final String message;
        if (args.length > 1) {
            final StringBuilder builder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                final String arg = args[i];
                builder.append(arg).append(' ');
            }
            message = builder.toString().trim();
        } else {
            message = defaultBanReason;
        }

        if (argumentParserBan.hasArgument('u')) {
            UserFileIO.addFileEntry(target, new Unban(sender.getName(), message));
            Bukkit.getOnlinePlayers().stream()
                    .filter(x -> x.hasPermission("teamutils.ban"))
                    .forEach(x -> Messenger.send(x, "teamutils.ban.notifyUnban", offlineTarget.getName(),
                            sender.getName(), message));
            return true;
        }

        // Remove target if online.
        final Player onlinePlayer = Bukkit.getPlayer(args[0]);
        if (onlinePlayer != null && onlinePlayer.isOnline()) {
            final String fmt = Messenger.getFormat("teamutils.ban.joinMessage");
            onlinePlayer.kickPlayer(String.format(fmt, message));
        }

        if (argumentParserBan.hasArgument('t')) {
            final String dateDiff = argumentParserBan.getArgument('t');
            final long diff = TimeHelper.parseTimeDifference(dateDiff);

            UserFileIO.addFileEntry(target, new TemporaryBan(sender.getName(), message,
                    System.currentTimeMillis() + diff));
            Bukkit.getOnlinePlayers().stream()
                    .filter(x -> x.hasPermission("teamutils.ban"))
                    .forEach(x -> Messenger.send(x, "teamutils.ban.notifyTempban", offlineTarget.getName(),
                            sender.getName(), message, TimeHelper.formatTimeDifference(diff)));
        } else {
            UserFileIO.addFileEntry(target, new PermanentBan(sender.getName(), message));
            Bukkit.getOnlinePlayers().stream()
                    .filter(x -> x.hasPermission("teamutils.ban"))
                    .forEach(x -> Messenger.send(x, "teamutils.ban.notifyBan", offlineTarget.getName(),
                            sender.getName(), message));
        }
        return true;
    }
}
