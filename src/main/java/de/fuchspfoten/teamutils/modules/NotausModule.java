package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * /notaus enables the whitelist, kicks all users, and stops the server.
 */
public class NotausModule implements CommandExecutor {

    /**
     * The parser for command arguments for /notaus.
     */
    private final ArgumentParser argumentParserNotaus;

    /**
     * The webhook URL.
     */
    private final String webhook;

    /**
     * Constructor.
     *
     * @param config The configuration.
     */
    public NotausModule(final ConfigurationSection config) {
        webhook = config.getString("modules.notaus.webhook");

        argumentParserNotaus = new ArgumentParser();
        argumentParserNotaus.addSwitch('f', "Supply this switch to perform an emergency restart");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.notaus")) {
            sender.sendMessage("Missing permission: teamutils.notaus");
            return true;
        }

        // Syntax check.
        /*args = */
        argumentParserNotaus.parse(args);
        if (argumentParserNotaus.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /notaus");
            argumentParserNotaus.showHelp(sender);
            return true;
        }

        if (!argumentParserNotaus.hasArgument('f')) {
            sender.sendMessage("Only dry-run: No -f flag supplied.");
            return true;
        }

        // Enable whitelist.
        Bukkit.setWhitelist(true);

        // Kick all players.
        Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer("Server closed"));

        // Perform a blocking HTTP request to notify the discord server.
        final String content = String.format("{\"content\":\"%1$s hat den Notaus-Schalter gedrueckt... @here\"}",
                sender.getName());
        //noinspection OverlyBroadCatchBlock
        try {
            final URL webhookURL = new URL(webhook);
            final HttpURLConnection connection = (HttpURLConnection) webhookURL.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Length", Integer.toString(content.length()));
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 " +
                    "Firefox/2.0.0.11");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            try (final DataOutputStream dos = new DataOutputStream(connection.getOutputStream())) {
                dos.writeBytes(content);
            }

            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                while (true) {
                    final String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    Bukkit.getLogger().severe("API message: " + line);
                }
            }

            Bukkit.getLogger().severe("API code: " + connection.getResponseCode());
            Bukkit.getLogger().severe("API response: " + connection.getResponseMessage());
            connection.disconnect();
        } catch (final IOException e) {
            Bukkit.getLogger().severe("Could not inform webhook of emergency shutdown!");
            e.printStackTrace();
        }


        // Stop the server after one second.
        TeamUtilsPlugin.scheduleTask(Bukkit::shutdown, 20L);

        return true;
    }
}
