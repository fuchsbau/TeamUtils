package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /cc clears the chat.
 */
public class ClearChatModule implements CommandExecutor {

    /**
     * The parser for command arguments for /cc.
     */
    private final ArgumentParser argumentParserCC;

    /**
     * Constructor.
     */
    public ClearChatModule() {
        Messenger.register("teamutils.clearchat.clearMessage");

        argumentParserCC = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.clearchat")) {
            sender.sendMessage("Missing permission: teamutils.clearchat");
            return true;
        }

        // Syntax check.
        args = argumentParserCC.parse(args);
        if (args.length != 0 || argumentParserCC.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /cc");
            argumentParserCC.showHelp(sender);
            return true;
        }

        // Clear the chat.
        for (final Player player : Bukkit.getOnlinePlayers()) {
            if (!player.hasPermission("teamutils.clearchat")) {
                for (int i = 0; i < 110; i++) {
                    player.sendMessage("§f");
                }
            }
        }

        // Announce the action.
        Bukkit.getOnlinePlayers().forEach(x -> Messenger.send(x, "teamutils.clearchat.clearMessage",
                sender.getName()));

        return true;
    }
}
