package de.fuchspfoten.teamutils.modules;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Implements the team compass.
 */
public class TeamCompassModule implements Listener {

    /**
     * Stored game modes.
     */
    private final Map<UUID, GameMode> gameModeStorage = new HashMap<>();

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getPlayer().hasPermission("teamutils.compass")) {
            // Ignore pressure plates.
            if (event.getAction() == Action.PHYSICAL) {
                return;
            }

            // Apply compass logic.
            final ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
            if (item != null && item.getType() == Material.COMPASS) {
                final UUID uid = event.getPlayer().getUniqueId();
                if (event.getPlayer().getGameMode() == GameMode.SPECTATOR) {
                    event.getPlayer().setGameMode(gameModeStorage.getOrDefault(uid, GameMode.SURVIVAL));
                } else {
                    gameModeStorage.put(uid, event.getPlayer().getGameMode());
                    event.getPlayer().setGameMode(GameMode.SPECTATOR);
                }
                event.setCancelled(true);
            }
        }
    }
}
