package de.fuchspfoten.teamutils.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.teamutils.TeamUtilsPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * /say &lt;message&gt; sends an official message to the chat.
 */
public class SayModule implements CommandExecutor {

    /**
     * The parser for command arguments for /say.
     */
    private final ArgumentParser argumentParserSay;

    /**
     * Constructor.
     */
    public SayModule() {
        Messenger.register("teamutils.say.format");

        argumentParserSay = new ArgumentParser();
        argumentParserSay.addSwitch('u', "Urgent message with spacing and alert sound");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("teamutils.say")) {
            sender.sendMessage("Missing permission: teamutils.say");
            return true;
        }

        // Syntax check.
        args = argumentParserSay.parse(args);
        if (args.length < 1 || argumentParserSay.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /say <message>");
            argumentParserSay.showHelp(sender);
            return true;
        }

        // Compose message.
        final StringBuilder builder = new StringBuilder();
        for (final String arg : args) {
            builder.append(arg).append(' ');
        }
        final String message = builder.toString().trim();

        // Is it an urgent message?
        final boolean urgent = argumentParserSay.hasArgument('u');

        // Send the message.
        if (urgent) {
            Bukkit.broadcastMessage("");
        }
        final String issuer = (sender instanceof ConsoleCommandSender) ? "Server" : sender.getName();
        Bukkit.getOnlinePlayers().forEach(p -> Messenger.send(p, "teamutils.say.format", issuer, message));
        if (urgent) {
            Bukkit.broadcastMessage("");
        }

        // Alert sound, if required.
        if (urgent) {
            for (int k = 0; k < 5; k++) {
                TeamUtilsPlugin.scheduleTask(() -> {
                    for (final Player player : Bukkit.getOnlinePlayers()) {
                        player.playSound(player.getEyeLocation(), Sound.ENTITY_GHAST_SCREAM, 1.0f, 1.0f);
                    }
                }, 7L * k);
            }
        }
        return true;
    }
}
